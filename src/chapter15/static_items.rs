use std::ops::Index;

#[allow(dead_code)]
trait Diet {
    const FOOD: &'static str;
    fn eat(&self);
}

impl Diet for i32 {
    const FOOD: &'static str = "Grapes";
    fn eat(&self) {
        static MEAL: &'static str = i32::FOOD;
        println!("I only eat {MEAL}");
    }
}

static F: &'static str = "Olive oil";

#[allow(dead_code)]
pub fn statics_demo() {
    println!("{F}");
    static D1: i32 = 42;
    D1.eat();
}

#[allow(dead_code)]
fn fun_1() -> i32 {
    ({
        #[inline]
        |x, y| x + y
    })(1, 2)
}

#[allow(dead_code)]
#[inline(always)]
fn fun_2() -> f64 {
    std::f64::consts::PI
}

#[allow(dead_code)]
#[inline(never)]
fn fun_3(x: bool, y: bool, z: bool) -> bool {
    x || y && z
}

#[allow(dead_code)]
pub fn lifetime_demo_1<'a>() -> &'a str {
    let x = "hello";
    let y: &'a str = &x;
    y
}

#[allow(dead_code)]
pub fn shadowing_demo() {
    let x = 21;
    if true {
        let x = 42;
        println!("{x}");
    }
    println!("{x}");
}

#[allow(dead_code)]
pub fn rust_shadowing_demo() {
    let x = 21;
    println!("{x}");
    let x: &str = "42";
    println!("{x}");
}

#[allow(dead_code)]
pub fn implicit_borrow_demo() {
    let arr = [1, 2, 3];
    let _a = arr[0];
    let _a = arr.index(0);
    let _a = (&arr).index(0);
}
