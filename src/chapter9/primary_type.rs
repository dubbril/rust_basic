#[allow(dead_code)]
pub fn float_demo() {
    let a: f32 = 1.0;
    let b: f64 = 2.0;
    //let sum = a + b;
    let sum = (a as f64) + b;
    println!("a + b = {}", sum);
}

#[allow(dead_code)]
pub fn point_of_no_return() {
    print!("A crab never return!");
}

#[allow(dead_code)]
pub fn point_of_no_return1() -> () {
    print!("A crab never return!");
}

#[allow(dead_code)]
pub fn point_of_no_u_return() -> ! {
    panic!("A crab never U-return");
}

#[allow(dead_code)]
// #![feature(is_some_and)] this syntax enable on nightly build
pub fn is_some_and_demo() {
    let (x1, x2, x3) = (Ok::<i32, ()>(10), Ok::<i32, ()>(-10), Err::<i32, _>("Huh?"));
    assert_eq!(x1.is_ok_and(|x| x > 0), true);
    assert_eq!(x2.is_ok_and(|x| x > 0), false);
    assert_eq!(x3.is_ok_and(|x| x > 0), false);
}
