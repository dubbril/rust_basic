#![allow(dead_code)]
pub fn slice_from_array() {
    let arr = [5, 10, 20, 42];
    let s1 = &arr as &[i32];
    println!("s1 = {s1:?}");
    let s2 = &arr[1..=2];
    println!("s2 = {s2:?}");
}

pub fn slice_from_array_2() {
    let mut arr = [5, 10, 20, 42];
    let m1 = &mut arr as &mut [i32];
    println!("m1 = {m1:?}");
    m1[1] = 11;
    println!("m1 = {m1:?}");
    println!("arr = {arr:?}");
}

pub fn slice_iteration() {
    let list = vec!["Do", "Re", "Mi", "Fa"];
    for v in &list {
        println!("{v}");
    }

    let slice = &list[..];
    for (i, s) in slice.iter().enumerate() {
        println!("{s}{}", "~".repeat(i + 1));
    }
}
