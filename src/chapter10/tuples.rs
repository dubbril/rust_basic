#![allow(dead_code)]
pub fn tuble_expessiongs() {
    let t0 = ();
    let t1 = (true,);
    let t2 = ('0', 2u8);
    let (name, title) = ("Ferris", "King Crab".to_string());
    let t3 = (name, "the", title);
    println!("{t0:?}; {t1:?}; {t2:?}; {t3:?}");

    let r1 = 1..=5;
    r1.for_each(|v| print!("{v}, "));
}

pub fn option_demo() {
    let b1: Option<bool> = None;
    let b2: Option<bool> = Some(true);
    println!("b1 = {b1:?}; b2 = {b2:?}");
    assert_ne!(b1, b2);
}
