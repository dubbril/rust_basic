#![allow(dead_code)]
pub fn array_demo() {
    let arr1 = [1, 2, 3, 5];
    println!("arr1 = {arr1:?}");
    println!("arr1[1] = {}", arr1[1]);

    let mut arr2 = [2u8, 4, 8, 10];
    println!("arr2 = {arr2:?}");
    arr2[2] = 88;
    println!("arr2 = {arr2:?}");
}

pub fn array_get() {
    let arr = ["Hell", "World", "Rust"];
    for i in [0, 2, 4] {
        if let Some(x) = arr.get(i) {
            println!("arr[{i}] == {x}");
        } else {
            println!("No value at index {i}");
        }
    }
}

pub fn array_expression() {
    let a1 = ["Feliz", "Ferris"];
    let a2 = ["Crab"; 4];
    println!("{a1:?}");
    println!("{a2:?}");
}
