use core::fmt::Display;
use std::fmt::Error;
use std::fmt::Formatter;

pub struct FortyTwo {}
impl Display for FortyTwo {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        write!(f, "{}", "42")
    }
}

#[derive(Debug)]
#[allow(dead_code)]
struct City {
    name: &'static str,
    pop: u64,
}

pub fn debug_trait_demo() {
    let ny = City {
        name: "New York",
        pop: 8_888_000,
    };
    println!("{ny:?}");
    println!("{ny:#?}");
}
