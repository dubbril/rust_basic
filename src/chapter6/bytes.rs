#[allow(dead_code)]
pub fn byte_literal() {
    let b1 = b'A';
    let b2 = 65;
    assert_eq!(b1, b2);
    if b1 == b2 {
        println!("Byte of 'A' is {}", b1)
    }
}
#[allow(dead_code)]
pub fn byte_strings() {
    let s1 = b"Hello' \"World\"!";
    let s2 = &[
        b'H', b'e', b'l', b'l', b'o', b'\'', b' ', b'"', b'W', b'o', b'r', b'l', b'd', b'"', b'!',
    ];
    assert_eq!(s1, s2);
    println!("s1 is '{:?}'", s1);
    println!("s2 is '{:?}'", s2);
}
#[allow(dead_code)]
pub fn raw_byte_strings() {
    let crab1 = br"You cannot teach a crab to walk straight";
    let crab2 = br##"~`!@#$%^&*(){}[]<>\|\
;:'"#,.\n\t\'\"\\?/\"##;
    let len1 = crab1.len();
    let len2 = crab2.len();
    println!("Crab1 ({len1}): {crab1:?}");
    println!("Crab2 ({len2}): {crab2:?}");
}
