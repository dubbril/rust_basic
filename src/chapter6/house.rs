#![allow(dead_code)]
mod dealer {
    pub use self::deck::deal_one;
    mod deck {
        pub fn deal_one() -> i32 {
            42
        }
    }
}

pub fn pub_use_demo() {
    let one = dealer::deal_one();
    println!("dealt = {one}");
}
