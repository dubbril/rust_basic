use std::io::Error;
use std::io::Write;
#[allow(dead_code)]
pub fn write_macro_demo() -> Result<(), Error> {
    let mut buf = Vec::new();
    let result = write!(&mut buf, "Hello {} times!", 42);
    println!("Result from function is {:?}", result);
    let text = String::from_utf8_lossy(&buf);
    println!("{text}");
    Ok(())
}
