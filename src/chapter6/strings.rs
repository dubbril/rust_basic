#[allow(dead_code)]
pub fn raw_strings() {
    let raw1 = r"Rust is your oyster.";
    let raw2 = r#"I love "oyster"."#;
    let raw3 = r##"Oyster is #1 \ 
the most loved food by the crabs."##;
    println!("{raw1}\n{raw2}\n{raw3}");
}

pub fn string_from() {
    let mut king = String::from("King ");
    king.push_str("Crab");
    king.push('!');
    println!("{king}");
}
