#[derive(Debug)]
#[allow(dead_code)]
struct Gene<'a, T: Copy> {
    dna: &'a [T],
}

#[allow(dead_code)]
impl<'a, T: Copy> Gene<'a, T> {
    fn first(gene: &Self) -> Option<T> {
        if let &[f, ..] = gene.dna {
            Some(f)
        } else {
            None
        }
    }
}

#[allow(dead_code)]
pub fn turbofish_demo<'a>() {
    let g = Gene::<'a, u8> { dna: &[1, 2, 3] };
    println!("{g:?}");

    let f = Gene::<'a, u8>::first(&g);
    println!("First element: {f:?}");
}
