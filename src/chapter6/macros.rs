use std::vec;
#[allow(dead_code)]
pub fn format_macro_demo() {
    let s1 = format!("{} {} {}", 1, 2, 3);
    println!("{}", s1);
    let s2 = format!("{0} {0}", 10);
    println!("{0}", s2);
    let s3 = format!("{a}", a = 666);
    println!("{s3}");
}

#[allow(dead_code)]
pub fn panic_macro() {
    // panic!();
    // panic!("Long time no see!");
    panic!("Well, it's been {} years.", 100);
}

#[allow(dead_code)]
pub fn assert_macros() {
    assert!(true == true, "True must be true");
    let t = 333;
    assert_eq!(333, t, "How can 333 be not {t}?");
    assert_ne!("hello", "world", "Is hello word?");
}

#[allow(dead_code)]
pub fn dbg_macro_demo() {
    let a = 963;
    let x = dbg!(a + 36);
    assert_eq!(x, 999);
    let b = String::from("Rust");
    let y = dbg!(&b);
    println!("{y}");
}

#[allow(dead_code)]
#[allow(unused_variables)]
pub fn todo_macro_demo(x: i32, y: f32) -> Option<bool> {
    todo!("to do or not to do?");
}

#[allow(dead_code)]
pub fn vec_macro_demo() {
    let _v = vec![1, 2, 3];
    println!("value of _v is {_v:?}");
    let _v = vec![10; 3];
    println!("value of _v is {_v:?}");
}
