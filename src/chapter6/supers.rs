#[allow(dead_code)]
fn crab() {
    println!("Just crab!");
}

#[allow(dead_code)]
mod sea {
    fn crab() {
        println!("Sea crab!");
    }

    pub mod world {
        pub fn crab() {
            super::super::crab();
            self::super::crab();
        }
    }
}
