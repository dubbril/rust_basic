use std::fmt::Display;
trait LiveForever<'a>
where
    Self::LifeForm: 'a + Copy + Display + From<&'a Self::LifeForm>,
{
    type LifeForm;
    fn make_life(creature: &'a Self::LifeForm) -> Self::LifeForm;
}
